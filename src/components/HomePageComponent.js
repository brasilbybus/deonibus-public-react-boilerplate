import React from 'react';
import PropTypes from 'prop-types';

const HomePageComponent = props => {
  return (
    <>
      <h1>{props.title}</h1>
      <h2>Boa sorte, Divirta-se!!! :)</h2>
    </>
  );
}

HomePageComponent.propTypes = {
  title: PropTypes.string
}

export default HomePageComponent;